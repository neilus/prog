#include <stdio.h>
#include <stdlib.h>

int main()
{
    int sorszam=0;
    char karakterlanc[10];
    FILE *fajl;// ez a fajl descriptor
    //megnyitom a fajlt olvasasra
    fajl = fopen("../ezegyfajl.txt","r");
    //addig olvasok soronkent egy stringet a fajbol, amig a fajl vegere nem erek
    while(fscanf(fajl,"%s\n",karakterlanc) != EOF){
        //kiirom a legutobb beolvasott stringet
        printf("%d\t%s\n",sorszam++,karakterlanc);
    }
    //bezarom a fajlt
    fclose(fajl);

    return 0;
}
