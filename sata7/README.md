Ez egy kis feladatocska, amit szabi ocsikem korrepetalasan szultunk ossze.

A feladat:
----------
Hozzon létre alacsony szintű fájlkezeléssel egy adat1.txt fájlt, majd írja ki ebbe a fájlba 100 és
200 közötti páros számok zárt halmazát! A létrehozott adat1.txt-t nyissa meg, és olvassa ki a
számokat, és ezek közül, amelyek 3-mal oszthatók, azokat írja be az adat2.txt, amelyek 7-tel
oszthatók, azokat pedig az adat3.txt-be! Mindkét esetben írja a fájl végére a lezárás előtti fájl
méretét! Ellenőrzésképpen olvassa vissza az adat2.txt-t és az adat3.txt tartalmát, majd írja ki a
képernyőre!


A dolgot CodeBlocks nevu C/C++ fejlesztokornyezetben csinaltuk, standard konzolos C project.
A program generalja a paros.txt, haros.txt es hetesek.txt-t, mint kimenetet, ok nem a megadott 
fajlnevet kovettem, mert nem volt elottem a szoveg, talan atirom... talan.
