;================================================================
;
;	proba02.ASM				HARDWARE HASZN�LATTAL
;
;	1. �rai AVR ASM M�SODIK progi
;
;	F7: Assemble,		Ctrl+F7: Assemble and Run		F11: Step
;
;----------------------------------------------------------------
/*
	DDRx  (8bit)	Data Direction (O:be, 1:ki) ( PORT ir�ny be�llit�s)
	PORTx (8bit)	1 PORT kimeneti �rt�k�nek a be�ll�t�sa (0: 0V, 1: 5V)
	PINx  (1bit)	1 l�b  kimeneti �rt�k�nek a be�ll�t�sa (0: 0V, 1: 5V)

=================================================================
*/

.include "m128def.inc"	; definici�s file 

.org	0x0				; kezd�c�m, innen indul a progi, RESET vektor kezdete
	jmp start			; start cimk�re elugrik

						; 0x0 cim �s a 0x100 c�m k�z�tt a megszak�t�sok vannak
						; ez�rt ezt a ter�letet ki kell hagyni
.org	0x100
start:

; ---- PORTB �s PORTD l�bainak BEmenetre �s KImenetre �ll�t�sa ---- ----
	LDI r16,  0b11110000; seg�dv�ltoz� a kimenetek be�ll�t�s�hoz
	nop
	out DDRB, r16		; B PORT fels� 4 bitje kimenet lesz, als� 4 bit bemenet
	nop
	out DDRD, r16		; D PORT fels� 4 bitje kimenet lesz, als� 4 bit bemenet
	nop

; ---- Sz�mol�s �s eredm�ny kirak�sa a PORTD-re ---- ---- ----	
	LDI r17,  5			; r17-be 5 decim�lisan
	nop
	LDI r18,  4			; r18-be 4 decim�lisan
	nop
	ADD r17, r18		; 2 regiszter �sszead�sa, eredm�ny r17-ban keletkezik
	nop
	out PORTD, r17		; r17 fels� 4 bitj�t kirakjuk, PORTD-re (!!! !!!)
	nop

	swap r17			; r17 fels� 4 �s als� 4 bitj�t fel kell cser�lni
	nop

	out PORTB, r17		; r17 als�  4 bitj�t kirakjuk, PORTB-re (!!! !!!)
	nop

stop:					; itt p�r�g a progi
		jmp stop
