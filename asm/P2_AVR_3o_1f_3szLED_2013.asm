
/*
======================================================

	3. �ra,	1. p�lda: 

		3 sz�n� LED - K�ZI - vez�rl�se, IT n�lk�l !!!

======================================================
*/

.include "m128def.inc"
.def tmp=r16		; seg�dv�ltoz�

.org 0x0			; kezd�c�m
	rjmp start

.org 0x100			; �tugrott erre a c�mre
start:

	LDI tmp, 0b_1000_0000	; 3 sz�n� LED RED l�ba itt (0x80 = 0b_1000_0000) 
	out DDRC, tmp			; PORTC legfels� bit kimenetre �ll�t�sa 

	LDI tmp, 0b_0000_1100	; 3 sz�n� LED GREEN, BLUE l�ba itt (0x0C = 0b_0000_1100)
	out DDRE, tmp			; PORTE megfelel� l�bainak kimenetre �ll�t�sa 


ciklus:	
		LDI	tmp, 0b_1000_0000	; RED
		out PORTC, tmp

		LDI	tmp, 0b_0000_1000	; GREEN
		out PORTE, tmp

		LDI	tmp, 0b_0000_0100	; BLUE
		out PORTE, tmp

	jmp ciklus

