/*
======================================================

	4. �ra, 2. p�lda:	
		stack m�k�d�s bemutat�sa

		View -> Memory ablak megnyit�sa
			|-> ADATMEM�RIA kiv�laszt�sa

	FONTOS !!!
		Innent�l kezdve, ha kell, ha nem, 
		stack-et mindig inicializ�ljuk!!!

		SUBRUTIN �s MEGSZAK�T�S (IT) kezel�sn�l kell!!!

====================================================== 
*/

.include "m128def.inc"
.def a=r16
.def b=r17
.def c=r18

.org 0x0
	rjmp start

.org 0x100
start:

	ldi	r25, HIGH(RAMEND)		; stack be�ll�t�s
	out	SPH, r25			; a mem�ria v�g�re teszi
	ldi	r25, LOW(RAMEND)		; a stack pointer-eket
	out 	SPL, r25
	clr 	r25

	ldi	a, 11		; "a" nev� regiszterbe (r16) bet�lt�nk 11-et
	ldi	b, 12		; "b" nev� regiszterbe (r17) bet�lt�nk 12-�t
	ldi     c, 13		; "c" nev� regiszterbe (r18) bet�lt�nk 13-at
	nop
	push 	a		; "a" �rt�k�t betessz�k a stack-be
	push 	b		; "b" �rt�k�t betessz�k a stack-be
	push	c		; "c" �rt�k�t betessz�k a stack-be

; verzi� 1. --- --- ---
	nop
	ldi	a, 1		; "a" nev� regiszterbe (r16) bet�lt�nk 1-et
	ldi	b, 2		; "b" nev� regiszterbe (r17) bet�lt�nk 2-et
	ldi	c, 3		; "c" nev� regiszterbe (r18) bet�lt�nk 3-et


	nop
	pop	c		; �rt�ket kivessz�k a STACK-b�l �s betessz�k "c"-ba (r18-ba)
	pop	b		; �rt�ket kivessz�k a STACK-b�l �s betessz�k "b"-ba (r17-ba)
	pop	a		; �rt�ket kivessz�k a STACK-b�l �s betessz�k "a"-ba (r16-ba)
	nop

; verzio 2. --- --- ---
;	nop
;	pop	a		; �rt�ket kivessz�k a STACK-b�l �s betessz�k "a"-ba (r16-ba)
;	pop	b		; �rt�ket kivessz�k a STACK-b�l �s betessz�k "b"-ba (r17-ba)
;	pop	c		; �rt�ket kivessz�k a STACK-b�l �s betessz�k "c"-ba (r18-ba)
;	nop


fociklus:			; itt p�r�g a progi a v�gtelens�gig
	jmp fociklus
