/* 
======================================================

	3. �ra, 5. p�lda:	

		- fut�f�ny �s delay MAKRO-val

		- LED fut�f�ny MAKRO-val

====================================================== 
*/

.include "m128def.inc"
.def tmp=r16
.def LED=r17

;--- MAKR�K ------------------------------------------

.macro LEDbeallitas
		ldi tmp, 0b11110000
		out DDRB, tmp
		out DDRD, tmp
.endmacro

.macro LEDvilagitas
		out  PORTB, tmp
		swap tmp
		out  PORTD, tmp
		swap tmp
.endmacro

.macro DELAY
	ldi R26, 0xFF
	ldi R25, 0xFF
	ldi R24, 0x0F

	delay_loop:
		nop
		dec  R26
		brne delay_loop
		dec  R25
		brne delay_loop
		dec  R24
		brne delay_loop
.endmacro

;---------------------------------------------

.org 0x0
	rjmp start

.org 0x100
start:

	LEDbeallitas

	ldi tmp, 0b00000001
	clc

loop:
		LEDvilagitas
		rol	 tmp
		delay
	jmp loop
