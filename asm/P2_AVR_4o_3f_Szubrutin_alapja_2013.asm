/*
======================================================

	4. �ra, 3. p�lda:	
				
		SZUBRUTIN m�k�d�s bemutat�sa (2 sz�m �sszead�sa)

			- �sszead�s sim�n
			- �sszead�s SZUBRUTIN alkalmaz�s�val
====================================================== 
*/

.include "m128def.inc"
.def a=r16
.def b=r17

.org 0x0
	rjmp start

.org 0x100
start:

			; -------------------------------------------
	ldi	r25, HIGH(RAMEND)		; stack be�ll�t�s
	out	SPH, r25				; a mem�ria v�g�re teszi
	ldi	r25, LOW(RAMEND)		; a stack pointer-eket
	out 	SPL, r25
	clr 	r25
			; -------------------------------------------

	ldi	a, 11		; "a" nev� regiszterbe (r16) bet�lt�nk 11-et
	ldi	b, 12		; "b" nev� regiszterbe (r17) bet�lt�nk 12-�t

	add	a, b		; sima �sszead�s

		; "a" �s "b" regiszterek eredeti �rt�keinek a vissza�ll�t�sa, 
		; hogy ugyanazt tudjam mutatni SZUBRUTIN-nal
	ldi	a, 11		; "a" nev� regiszterbe (r16) bet�lt�nk 11-et
	ldi	b, 12		; "b" nev� regiszterbe (r17) bet�lt�nk 12-�t

	call subrutine_osszeadas	; subrutune_osszeadas megh�v�sa

fociklus:			; itt p�r�g a progi a v�gtelens�gig
	jmp fociklus

; ------- Szubrutinok IDE -----------------------------

subrutine_osszeadas:	; �sszead�s szubrutin le�r�sa
	add a, b			; "a" �s "b" nev� regiszterek �sszead�sa
ret
