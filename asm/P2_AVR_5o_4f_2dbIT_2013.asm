/*
================================================================== 

	5. �ra, 4. p�lda:	

		Megszak�t�s, Interrupt, IT m�k�d�s bemutat�sa

		KETT� INTERRUPT fut�s p�rhuzamosan

			1. IT: LED fut�f�ny megval�s�t�sa IT0-�s INTERRUPT-tal
			2. IT: 7 szegmenses szamol�s megval�s�t�sa IT1-es INTERRUPT-tal
				   ( pontos 1 szekundum-os villogtat�s )
================================================================== 
*/

.include "m128def.inc"
.def tmp =r16	; seg�dv�ltoz�
.def led =r18	; LED meghajt� regiszter
.def lassito=r19; IT0 interrupt SW lass�t�
;------------------------------------------------------------

.org 0x0		; ezen a c�men indul a PROGI
	rjmp start

.org 0x18
	rjmp IT1	; 'IT1 Compare Match A' megszak�t�s jelz�se az 0x18-as c�men van, 
				; ha a IT1 2 BYTE-os sz�ml�l� el�r egy sz�mot (amikor a sz�mmal egyenl� lesz),
				; akkor null�zza mag�t a 2 BYTE-os sz�ml�l�, �s ut�na ad egy IT-t 0x18 c�mre.
				; DSh. 57. oldal
.org 0x20
	rjmp IT0	; 'IT0 overflow' megszak�t�s bej�vetel�nek jelz�se az 0x20-as c�men van, 
				; ha a IT0 8 bites sz�ml�l�ja t�lcsordul, akkor ad egy IT-t 0x20 c�mre.
				; DSh. 57. oldal

.org 0x100		; progi itt kezd�dik
start:
				
			; --- STACK inicializ�l�sa ------------------
		ldi	r25, HIGH(RAMEND)	; stack be�ll�t�s
		out	SPH, r25			; a mem�ria v�g�re teszi
		ldi	r25, LOW(RAMEND)	; a stack pointer-eket
		out 	SPL, r25
		clr 	r25
	
			; --- LED-eket vez�rl� PORT-ok be�ll�t�sa PORTB, PORTD ---
		ldi tmp,   0b11110000	; PORT-ok fels� 4 bitje kimenetre �ll�tani (LED)
		out DDRB,  tmp
		out DDRD,  tmp

			; --- IT0 kezdeti be�ll�t�sa ----------------
		LDI tmp, 	0b0000_0111	; seg�dv�ltoz� a be�ll�t�shoz
		out TCCR0,	tmp		; 103. oldal DSh.-b�l, IT0-ra CLK/1024 el�oszt�st �ll�tok be
						; als� 3 bit 111 
						; "16000000 Hz / 1024 / 256 (OF) = 61,0351" soha nem lesz eg�sz sz�m

			; --- IT1 kezdeti be�ll�t�sa ----------------
		LDI tmp, 	0b0000_1101 ; 135. oldal DSh.-b�l,
		out TCCR1B, tmp			; IT1-re CLK/1024 el�oszt�st �ll�tok be; als� 3 bit '101', 
								; alulr�l a 4. bit CTC (Clear Timer on Compare Match) m�d

		LDI tmp, 	0b0000_0000	; 131. oldal DSh.-b�l, IT1-re ezt a regisztert is COMPARE m�dra �ll�tom,
		out TCCR1A, tmp			; alulr�l a 0. �s a 1. bitet kell n�zni


		ldi tmp, HIGH(15624)	; "SZ�M-1"-et kell beadni, mert null�r�l kezd sz�molni (!!!)
		out OCR1AH, tmp		; "16000000 Hz / 1024 = 15625 Hz (IT1/sec)" => "SW-es oszt� kell 15624-re" 
		ldi tmp, LOW(15624)	; ha a IT1 2 BYTE-os sz�ml�l� el�r egy sz�mot (amikor a sz�mmal egyenl� lesz),
		out OCR1AL, tmp		; akkor null�zza mag�t a 2 BYTE-os sz�ml�l�, �s ut�na ad egy IT-t 0x18 c�mre.
					; PONTOS 1 m�sodperces villog�st kapunk IT1-el (!!!)

		LDI tmp, 	0b0001_0001	; 'IT0 OVRFlow (TOIE0)' m�d �s 'IT1 COMPARE CTC (OCIE1)' m�d
		out TIMSK,	tmp			; INTERRUPT-ok enged�lyez�s�t �ll�tom be

		ldi lassito, 61			; lassito kinull�z�sa
		SEI						; proci engedi befogadni a bej�v� IT-t

CIKLUS:			; ITT P�R�G A PROGI, l�tsz�lag nem csin�l semmit
	sleep		; kisfoyaszt�s� �llapot
	jmp CIKLUS

; --- Megszak�t�sok ----------------------------------------------------------------- 
	; L�THAT�, hogy az IT0-�t �s IT1-et nem a F�progib�l h�vjuk, hanem egy k�ls� HW 
	; kezdem�nyezi, �s ut�na, ha a PROCI elfogadja a megszak�t�st (SEI), akkor lefut
	; az interrupt, ami j�n.


IT0:					; CSAK minden 16. IT0 megj�vetelekor v�lt a LED
	dec lassito			; 1-el cs�kkentj�k a lassito �rt�k�t
	brne cimke1			; ugrik a cimke1-re, ha lassito nem "0"  
	ldi lassito, 61


	ldi	tmp,	128
	eor	led,	tmp		; XOR-ral legfels� LED villogtat�sa
	out	PORTD,	led 

cimke1:

reti					; RETI = RET + SEI
; ------------------------------------------------------------------------------------

IT1:
	ldi	 tmp,	1
	eor	 led,	tmp		; XOR-ral legals� LED villogtat�sa
	swap led
	out	 PORTB,	led 
	swap led
reti

