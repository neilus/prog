;================================================================
;
;	proba01.ASM			Pr�ba ASM
;
;	1. �rai AVR ASM progi
;
;	soronk�nti komment (; ut�na minden komment)
;
;	F7: Assemble,		Ctrl+F7: Assemble and Run		F11: Step
;
;----------------------------------------------------------------
/*
	Blokkonk�nti komment ( mint C-ben )

-----------------------------------------------------------------

	FELADAT:

		- bels� v�ltoz�knak �rt�ket adunk (r01-r15, ill. r16-r31)
		- bels� v�ltoz�kkal m�veletet v�gz�nk
		- bels� v�ltoz�kon manipul�lunk

=================================================================
*/

.include "m128def.inc"	; definici�s file 

						; Direkt�v�k, besz�des v�ltoz�k (!!!)
.def a=r16				; elnevezem az r16-os regisztert a-nak 
.def b=r17				; elnevezem az r17-os regisztert b-nak

.org	0x0				; kezd�c�m, innen indul a progi, RESET vektor kezdete
	jmp start			; start cimk�re elugrik

						; 0x0 cim �s a 0x100 c�m k�z�tt a megszak�t�sok vannak
						; ez�rt ezt a ter�letet ki kell hagyni

.org	0x100
start:

	nop					; nem csin�lok semmit (NO OPERATION)
	nop
	nop
	nop

; ---- ---- ---- �rt�kad�s ---- k�zvetlen�l r16-r31 !!! ---- ---- ----
	nop
	LDI r16, 2				; r16-os regiszternek �rt�ket adok, 2 decim�lisan
	nop
	LDI r17, 0b00000010		; r17-es regiszternek �rt�ket adok, 2 bin�risan
	nop
	LDI r18, 0b_0000_0010	; r18-as regiszternek �rt�ket adok, 2 bin�risan (l�that�)
	nop
	LDI r19, 0x02			; r19-es regiszternek �rt�ket adok, 2 hex�ban
	nop
	LDI r20, 02				; r20-as regiszternek �rt�ket adok, 2 okt�lisan
	nop

; ---- ---- ---- �rt�kad�s ---- k�zvetve r01-r15 !!! ---- ---- ----
	nop
	LDI r16, 3				; r16-os regiszternek �rt�ket adok, 3 decim�lisan
	nop
	MOV r2, r16				; r16=3 �rt�k�t bepakolom r02-be (k�zvetve)

; ---- ---- ---- �sszead�s, Kivon�s M�VELETEK ---- ---- ----
	NOP
	ADD r16, r17			; egyszer� �sszead�s r16-ban keletkezik az eredm�ny
	nop
	SUB r18, r19			; egyszer� kivon�s   r18-ben keletkezik az eredm�ny
	nop

; ---- ---- ---- Szorz�s M�VELET ---- ---- ----
	LDI r16, 4				; r16-ba 4 decim�lisan
	nop
	LDI r17, 5				; r17-be 5 decim�lisan
	nop
	MUL r16, r17			; egyszer� szorz�s r0-ban keletkezik az eredm�ny (!!!)
	nop

; ---- ---- ---- BITM�VELETEK ---- ---- ---- 
	LDI r20, 0b00000101		; r20-ba 5 bin�risan
	nop
	lsr r20					; Logical Shift Right
	nop 
	lsl r20					; Logical Shift Left
	nop

	rol r20					; Rotate LEFT   t. CARRY
	nop
	ror r20					; Rotate RIGHT  t. CARRY
	nop
	clr r20					; BITt�rl�s
	nop

; ---- ---- ---- ---- ---- ---- ---- ---- 
	LDI r20, 0b00000101		; r20-ba  5 bin�risan
	LDI r21, 0b00001111		; r20-ba 15 bin�risan
	nop

	AND	r20, r21		; bitenk�nti �S
	nop
	OR	r20, r21		; bitenk�nti VAGY
	nop
	EOR	r20, r21		; bitenk�nti KIZ�R� VAGY
	nop

stop:					; itt p�r�g a progi a v�gtelens�gig
	jmp stop
