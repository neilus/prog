;=============================================================
;
;	1. p�lda: 
;			- GOMB-ok �s LED-ek kezel�se �mlesztve
;
;=============================================================

.include "m128def.inc"
.def tmp =r16		; seg�dv�ltoz� elnevez�s
.def gomb=r17		; GOMB �llapot�t lek�rdez� regiszter
.def led =r18		; LED meghajt� regiszter

.org 0x0
	rjmp start
.org 0x100

start:

LEDbeallit:			; LED-eket vez�rl� PORT-ok be�ll�t�sa PORTB, PORTD
	ldi tmp,   0b11110000	; fels� 4 bit 1-es, als� 4 bit 0-�s
	out DDRB,  tmp			; fels� 4 bit KI-m, als� 4 bit BE-m
	out DDRD,  tmp			; fels� 4 bit KI-m, als� 4 bit BE-m

GOMBbeallit:		; GOMB-okat vez�rl� PORTG be�llit�sa bemenetre
	ldi	tmp,   0b00000000	; temp v�ltoz� bemenetre �ll�tva
	sts	DDRG,  tmp			; PORTG bemenet

CIKLUS:

	GOMBolvas:
		lds  gomb,  PING		; gomb nev� reg-be olvassuk a gombok �llapotot
		andi gomb,	0b00011111	; ki kell maszkolni, hogy a t�bbi ne zavarjon
		mov  led, gomb			; led nev� reg-be bet�ltj�k a gomb �rt�k�t

	LEDvezerles:
		out  PORTD, led		; led �rt�k�t kitessz�k a PORTD-re
		swap led		; felcser�lj�k led nev� regiszter als� �s fels� 4 bitj�t
		out  PORTB, led		; led �rt�k�t kitessz�k a PORTB-re

jmp CIKLUS
