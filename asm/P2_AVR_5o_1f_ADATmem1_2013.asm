/*
==========================================================
	5. �ra, 1. feladat

		ADAT-MEM�RIA felt�lt�se sz�mokkal

==========================================================
*/

.include "m128def.inc"
.def szam =r16	; sz�m �rt�ke
.def db	  =r17	; h�ny darab sz�m legyen a mem�ri�ban


.org 0x0
	rjmp start

.org 0x100
start:
	; --- STACK inicializ�l�sa - ha haszn�ljuk, ha nem, MEGCSIN�LJUK ---
		ldi	r25, HIGH(RAMEND)	; stack be�ll�t�s
		out	SPH, r25			; a mem�ria v�g�re teszi
		ldi	r25, LOW(RAMEND)	; a stack pointer-eket
		out SPL, r25
		clr r25

	ldi szam, 10	; amilyen �rt�kt�l kezd�d�en
	ldi db,    5	; h�ny sz�m legyen benne, h�nyszor fusson le a ciklus

	; - Z�r�jelben l�v� MEM�RIA C�M-t�l kezdve felt�ltj�k a mem�ri�t egy sz�mmal
	ldi ZL, low(0x203) 	; z�r�jelbe a TELJES C�M ahonnan kezd�nk (LOW  r�sz)
	ldi ZH, high(0x203)	; z�r�jelbe a TELJES C�M ahonnan kezd�nk (HIGH r�sz)	
	; - low �s high szavak majd megoldj�k, hogy hogyan kell sz�tbontani a 
	;   teljes sz�mot. (Felt�ve, ha a dupla c�m egym�s mellett van a t�rban.)

memoria_feltolt:
				; �gy m�r 2 BYTE-os Z MEM�RIA c�met lehet egyben kezelni
;		st Z,  szam	; aktu�lis sz�mot MINDIG ugyanabba a Z MEM�RIA ter�letre t�ltj�k be (VIGY�ZAT!!!)
		st Z+, szam	; aktu�lis sz�mot bet�ltj�k, �s egyben l�ptetj�k ut�na Z-t a MEM�RI�ban (J�!!!)

		inc szam	; aktu�lis �rt�k n�vel�se (sz�m+1)
		dec db		; db_sz�m cs�kkent�se (ciklus v�ltoz� cs�kkent�se, db-1)
	brne memoria_feltolt	; UGRIK a 'memoria_feltolt:' cimk�re, ha 'NEM 0' a db, teh�t m�g kell ciklusban lenni.

vege:
	rjmp vege
