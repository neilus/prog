/*
================================================================== 

	5. �ra, 3. p�lda:  (ISM�TL�S)	
				
		Megszak�t�s, Interrupt, IT0 m�k�d�s bemutat�sa

		LED fut�f�ny megval�s�t�sa IT0-�s INTERRUPT-tal

================================================================== 
*/

.include "m128def.inc"
.def tmp =r16		; seg�dv�ltoz�
.def led =r18		; LED meghajt� regiszter
;------------------------------------------------------------

.org 0x0		; Ezen a c�men indul a PROGI
	rjmp start

.org 0x20
	rjmp IT0	; IT0 overflow megszak�t�s bej�vetel�nek jelz�se az 0x20-as c�men van, 
			; ha a IT0 8 bites sz�ml�l�ja t�lcsordul, akkor ad egy IT-t 0x20 c�mre.
			; DSh. 57. oldal

.org 0x100
start:
				
			; --- STACK inicializ�l�sa ------------------
		ldi	r25, HIGH(RAMEND)	; stack be�ll�t�s
		out	SPH, r25		; a mem�ria v�g�re teszi
		ldi	r25, LOW(RAMEND)	; a stack pointer-eket
		out 	SPL, r25
		clr 	r25
			; --- STACK inicializ�l�s V�GE --------------
	
			; --- LED-eket vez�rl� PORT-ok be�ll�t�sa PORTB, PORTD ---
		ldi tmp,   0b11110000	; PORT-ok fels� 4 bitje kimenetre �ll�tani	
		out DDRB,  tmp
		out DDRD,  tmp
	
		ldi led, 0b0000_0001	; LED v�ltoz�ba a kezdeti sz�m
		clc
			; --- IT0 kezdeti be�ll�t�sa ----------------
		LDI tmp, 	0b0000_0111	; seg�dv�ltoz� a be�ll�t�shoz
		out TCCR0,	tmp		; 105. oldal DSh.-b�l, CLK/1024 el�oszt�st �ll�tok be
		LDI tmp, 	0b0000_0001
		out TIMSK,	tmp		; IT0-�s INTERRUPT enged�lyez�st �ll�tok be,
						; ha az IT0 8 bites sz�ml�l�ja t�lcsordul, 
						; akkor ad egy INTERRUPT-ot

		SEI				; proci engedi befogadni a bej�v� IT-t
			; --- IT0 kezdeti be�ll�t�s V�GE ----------------

CIKLUS:			; ITT P�R�G A PROGI, l�tsz�lag nem csin�l semmit
	sleep		; kisfoyaszt�s� �llapot
	jmp CIKLUS


; --- Megszak�t�s IT0 ------------------------ 
				; L�THAT�, hogy az IT0-�t nem a F�progib�l h�vjuk,
				; hanem egy k�ls� HW kezdem�nyezi, �s ut�na,
				; ha a PROCI elfogadja a megszak�t�st (SEI), 
				; akkor lefut az IT0-hoz tartoz� IT rutin.
IT0:
				; CSAK minden 16. IT megj�vetelekor l�p a LED
	dec tmp			; 1-el cs�kkentj�k a tmp �rt�k�t
	brne cimke1		; ugrik a cimke1-re, ha tmp nem "0"  
	ldi tmp, 16		; ha tmp=0, akkor egyb�l kezdeti �rt�ket adni neki, itt tmp=16 

	out  PORTB, led 	; kitenni a LED v�ltoz� fels� 4 bitj�t, a PORTB fels� 4 bitj�re (LED meghajt�s)
	swap led		; fels� 4 �s als� 4 bit csere
	out  PORTD, led 	; kitenni a LED v�ltoz� eredeti als� 4 bitj�t, a PORTD fels� 4 bitj�re (LED meghajt�s)
	swap led		; vissza�ll�tjuk a sz�mot, hogy ut�na tudjuk l�ptetni

	rol	 led		; LED, ROTATE LEFT th. CARRY

cimke1:

reti				; RETI = RET + SEI

