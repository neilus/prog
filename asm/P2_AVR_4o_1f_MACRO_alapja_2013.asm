/*
======================================================

	4. �ra, 1. p�lda:	
				
		MACRO m�k�d�s bemutat�sa (2 sz�m �sszead�sa)

			- �sszead�s sim�n
			- �sszead�s macro alkalmaz�s�val
====================================================== 
*/

.include "m128def.inc"
.def a=r16
.def b=r17

.macro macro_osszeadas	; macro_osszeadas le�r�sa
	add a, b	; csak egy sorb�l �ll
.endmacro

.org 0x0
	rjmp start

.org 0x100
start:

	ldi	a, 11		; "a" nev� regiszterbe (r16) bet�lt�nk 11-et
	ldi	b, 12		; "b" nev� regiszterbe (r17) bet�lt�nk 12-�t

	add	a, b		; sima �sszead�s

		; "a" �s "b" regiszterek eredeti �rt�keinek a vissza�ll�t�sa, 
		; hogy ugyanazt tudjam mutatni macroval
	ldi	a, 11		; "a" nev� regiszterbe (r16) bet�lt�nk 11-et
	ldi	b, 12		; "b" nev� regiszterbe (r17) bet�lt�nk 12-�t

	macro_osszeadas		; macro_osszead�s megh�v�sa

fociklus:			; itt p�r�g a progi a v�gtelens�gig
	jmp fociklus
