;=================================================
;
;	3. �ra, 4. feladat
;
;		- fut�f�ny �s delay �mlesztve
;
;=================================================

.include "m128def.inc"

.def tmp=r16
.def LED=r17

.org 0x0				; kezd�c�m, itt indul a progi
	jmp start			; jmp utas�t�s 0x0 c�men van

.org 0x100				; c�m
start:					; start c�mke 0x100 c�men van

	ldi tmp, 0b11110000		; seg�dv�ltoz� a PORTir�ny be�ll�t�s�hoz
	out DDRB, tmp			; PORTB fels� 4 bitje 1 (KIm), als� 4 bit 0 (BEm)
	out DDRD, tmp			; PORTD fels� 4 bitje 1 (KIm), als� 4 bit 0 (BEm)

	ldi tmp, 0b_0000_0001	; 1-et t�lt�nk be a TMP nev� regiszterbe 
							; (alapb�l egy LED vil�g�t) {Mi van, ha 3-at t�lt�nk be?}
	clc						; CLEAR Carry (Carry t�rl�s)

loop:
		out  PORTB, tmp ; TMP-et betessz�k PORTB-be ( PORTB fels� 4 bitje a l�nyeg LED!)
		swap tmp		; fels� 4 �s als� 4 bit csere
		out  PORTD, tmp ; TMP eredeti als� 4 bitj�t betessz�k PORTD fels� 4 bitj�re 
						; (PORTD fels� 4 bitje a l�nyeg LED!)

		swap tmp		; vissza�ll�tjuk a sz�mot, hogy ut�na tudjuk l�ptetni		

		rol	 tmp		; TMP, ROTATE LEFT th. CARRY

	;--------------------------------------------------------------
				ldi R26, 0b_1111_1111	; regiszterekbe beadok valamilyen sz�mot
				ldi R25, 0b_1111_1111
				ldi R24, 0b_0000_1111

			delay_loop:
					dec  R26		; lefele sz�mol, ha 0 lesz, akkor Z flag=1
					brne delay_loop ; Z flag-et n�zi, ha Z flag=0, akkor ugrik, 
									; mert akkor a r26 sz�m nem 0
					dec  R25
					brne delay_loop
	
					dec  R24
					brne delay_loop
	;--------------------------------------------------------------

	jmp loop
