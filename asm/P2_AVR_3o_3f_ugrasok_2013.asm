

;=============================================================
;
;	3. �ra, 3. p�lda: 
;
;		- UGR�SOK:
;
;			- felt�tel n�lk�li ugr�s (pl.: JMP, RJMP)
;
;			- felt�teles ugr�sok (pl.: BRNE, BREQ, BRLT)
;
;=============================================================

.include "m128def.inc"	; definici�s file

.def osszeg=r16			; regiszter elnevez�sek
.def i=r17
.def N=r18

.org 0x0
	rjmp start

.org 0x100

start:
;======================================================================
; 111 === 1. FELADAT: r5 regisztert v�gtelens�gig n�velj�k === 111
		
	ldi	r17, 0	; ideiglenesen haszn�lt r17 regiszter
	mov r5, r17

ciklus0:
	inc	r5
;	jmp	ciklus0	; v�gtelens�gig p�r�g 
				; (  jmp: abszol�t c�mz�s, 
				;   rjmp: relat�v  c�mz�s (korl�tozott c�m�tfog�s) )

;======================================================================
; 222 === 2. FELADAT: 0-10-ig �sszeadni a sz�mokat, regiszter elnevez�ssel === 222
	
	ldi osszeg,	0	; r16 reg., �sszeg kezdetben 0 
	ldi i,		0	; r17 reg., seg�dv�ltoz�
	
ciklus1:

	add  osszeg, i
	inc  i

	CPI	 i, 0b00001010	; egy regisztert �s egy sz�mot hasonl�t �ssze	
						; ugr� utas�t�s el�tti vizsg�lat (KELL !!!)
	BRNE ciklus1		; ugrik a ciklus1-re, ha nem egyenl� i<>sz�m, 
						; ami el�tte van, annak az eredm�ny�t �rt�keli ki
						; BRNE Z (zero) regisztert figyeli
	nop;
	nop;

;======================================================================
; 333 === 3. FELADAT: 0-N-ig �sszeadni a sz�mokat, regiszter elnevez�ssel === 333

	ldi osszeg,	0
	ldi i,		0
	ldi N,		10

ciklus2:

	add  osszeg, i
	inc	 i

	CP   i, N			; k�t regisztert hasonl�t �ssze	
						; ugr� utas�t�s el�tti vizsg�lat (KELL !!!)
	BREQ ciklus2vege	; ugrik a ciklus2vege cimk�re, ha i=N, 
						; ami el�tte van, annak az eredm�ny�t �rt�keli ki
						; BREQ Z (zero) regisztert figyeli
	JMP ciklus2

ciklus2vege:

	nop;
	nop;

;======================================================================
; 444 === 4. FELADAT: 0-N-ig �sszeadni a sz�mokat === 444

	ldi osszeg,	0
	ldi i,		0
	ldi N,		10
	
ciklus3:

	add  osszeg, i
	inc  i

	CP 	 i, N		; k�t regiszter �szehasonl�t�sa
	BRLT ciklus3	; ugrik a ciklus3-ra, ha i<N, 
					; ami el�tte van, annak az eredm�ny�t �rt�keli ki
					; BRLT  Z �s V regisztereket figyeli

	nop;
	nop;

vege:
	jmp vege

