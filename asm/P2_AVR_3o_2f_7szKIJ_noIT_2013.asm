
/*
==========================================================

	3. �ra,  2. feladat		
	
		- 7 szegmenses kijelz� vez�rl�se

		- FIGYELEM IT n�lk�l, csak - K�ZI - meghajt�s (!!!)

==========================================================
*/

.include "m128def.inc"

.org 0x0
		rjmp	start

.org 0x100
start:

	ldi	r16,  0xFF	; kimenetre �ll�tom PORTA-t (0xFF = 0b_1111_1111)
	out DDRA, r16	; ez vez�rli a 7 szegmenses kijelz�t

loop:
		ldi	r16, 0b1_000_0000	; 0b 1       000	   0000
		out PORTA, r16			; 	 Enable, Szegmens, Sz�m (als� 7 szegmenses kij.)
		nop;

	;	ldi	r16, 0b1_001_0001	; 0b 1       001       0001
	;	out PORTA, r16			; 	 Enable, Szegmens, Sz�m
	;	nop;

	;	ldi	r16, 0b1_010_0010	; 0b 1       010       0010
	;	out PORTA, r16			; 	 Enable, Szegmens, Sz�m
	;	nop;

	;	ldi	r16, 0b1_011_0011	; 0b 1       011       0011
	;	out PORTA, r16			; 	 Enable, Szegmens, Sz�m (fels� 7 szegmenses kij.)
	;	nop;

	;	ldi	r16, 0b1_100_0000	; 0b 1       100       xxxx
	;	out PORTA, r16			; 	 Enable, Szegmens, Sz�m (kett�spont)
	;	nop;


	jmp loop

/*
	LE�R�S:

		7 szegmenses kijelz� a PORTA-n van

			PORTA 	 0b 1       000	   	  0111
						Enable, Szegmens, Sz�m

			Fels� BIT. (7. BIT)
			1	: Szegmens     Enable
			0	: Szegmens NOT Enable

			6., 5., 4. BIT
			000 : 0., els�	   als� szegmens van megc�mezve
			001 : 1., m�sodik  als� szegmens van megc�mezve
			010 : 2., harmadik als� szegmens van megc�mezve
			011 : 3., fels� szegmens van megc�mezve
			100 : a kett�spont  (:)  van megc�mezve

			3., 2., 1., 0. BIT
			0-9-ig kik�ldi a sz�mot a kijelz�nek
			0000 : 0
			0001 : 1
			0010 : 2
			0011 : 3
			0100 : 4
			0101 : 5
			0110 : 6
			0111 : 7
			1000 : 8
			1001 : 9
			t�bbi sz�m-�llapot tiltott

*/
