;=============================================================
;
;	3. p�lda: 
;				- GOMB-ok �s LED-ek kezel�se MAKR�-val
;
;=============================================================

.include "m128def.inc"
.def tmp =r16				; seg�dv�ltoz� elnevez�se
.def gomb=r17				; GOMB �llapot�t lek�rdez� regiszter
.def led =r18				; LED meghajt� regiszter
;------------------------------------------------------------

.macro LEDbeallit			; LED-eket vez�rl� PORT-ok be�ll�t�sa PORTB, PORTD
	ldi tmp,   0b11110000	; fels� 4 bit 1-es, als� 4 bit 0-�s
	out DDRB,  tmp			; fels� 4 bit KI-m, als� 4 bit BE-m
	out DDRD,  tmp			; fels� 4 bit KI-m, als� 4 bit BE-m
.endmacro

.macro GOMBbeallit			; GOMB-okat vez�rl� PORTG be�llit�sa bemenetre
	ldi	tmp,   0b00000000	; temp v�ltoz� bemenetre �ll�tva
	sts	DDRG,  tmp			; PORTG bemenet
.endmacro

.macro GOMBolvas
		lds  gomb,  PING		; gomb nev� reg-be olvassuk a gombok �llapotot
		andi gomb,	0b00011111	; mivel 5 gomb van als� 5 biten, 
.endmacro					; ki kell maszkolni, hogy a t�bbi ne zavarjon

.macro LEDvezerles
		out  PORTD, led
		swap led
		out  PORTB, led
.endmacro
;------------------------------------------------------------

.org 0x0
	jmp start
.org 0x100

start:

LEDbeallit			; !!! megh�v�s
GOMBbeallit			; !!! megh�v�s

CIKLUS:

	GOMBolvas		; !!! megh�v�s
	mov  led, gomb
	LEDvezerles		; !!! megh�v�s

jmp CIKLUS
