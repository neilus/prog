/*
==========================================================
	5. �ra, 2. feladat

		MEM�RIA felt�lt�s sz�mokkal

		MEM�RIA adatainak �r�sa, olvas�sa, m�sol�sa

==========================================================
*/
.include "m128def.inc"

.def szam  = r16
.def darab = r17
.def osszeg= r18

.org 0x0
	rjmp start

.org 0x100
start:

	; --- STACK inicializ�l�sa - ha haszn�ljuk, ha nem, MEGCSIN�LJUK ---
		ldi	r25, HIGH(RAMEND)	; stack be�ll�t�s
		out	SPH, r25			; a mem�ria v�g�re teszi
		ldi	r25, LOW(RAMEND)	; a stack pointer-eket
		out SPL, r25
		clr r25

	ldi XH, 0x01	; 0x102 sz�mot be�rok X fels� c�m�be (BAJLOD�SAN)
	ldi XL, 0x02 	; 0x102 sz�mot be�rok X als�  c�m�be (BAJLOD�SAN)

	ldi szam, 123	; 123-at DECIM�LISAN

	st	X, szam		; 'szam' �rt�k�t bet�ltj�k Z-be, VIGY�ZAT (!!!) NEM L�P TOV�BB A Z
	nop

; 111 --- DATA Mem�ri�ba IR�S --------------------------------
; 1-5-ig be�rjuk a sz�mokat kezdve a 0x106 c�mr�l 

	ldi XH, high(0x106)	; innen kezd�nk �rni a mem�ri�ba (0x106), 2 Byte-os c�m,
	ldi XL, low(0x106)	; low, high sz�cska majd megoldja a sz�tv�logat�st

	ldi szam, 1		; els� sz�m 1-es
	ldi darab,5		; 5 db. sz�mot irunk a memori�ba


ciklus1:
		st 	X+, szam	; bele�rjuk a mem�ri�ba a sz�m-ot, 
						; ut�na eggyel n�velj�k a mem�ria c�m�t (egyszerre!)
		inc szam		; n�velj�k a sz�m-ot, 
		dec darab		; darabsz�mot cs�kkentj�k

	brne ciklus1		; visszaugrik 'ciklus1'-re, am�g 'darab <> 0'
						; ha 'darab=0', akkor k�szen vagyunk

; 222 --- DATA Mem�ri�b�l OLVAS�S ---------------------------
; A 0x106-os c�mt�l kezd�d�en kiolvassuk a sz�mokat 1-5-ig (amit el�bb be�rtunk), 
; �s �sszeg-�ket berakjuk az 'osszeg' regiszterbe

	ldi XH, high(0x106)	; innen kezd�nk olvasni a mem�ri�b�l (0x106), 2 Byte-os c�m,
	ldi XL, low(0x106)	; low, high sz�cska majd megoldja a sz�tv�logat�st

	clr osszeg		; alapesetben az �sszeg=0
	ldi darab, 5	; 1-5-ig adjuk �ssze a sz�mokat

ciklus2:

		ld szam, X+		; kiolvassuk a mem�ri�b�l a sz�m-ot, 
						; ut�na eggyel n�velj�k a mem�ria c�m�t (egyszerre!)
		add osszeg, szam; hozz�adjuk az aktu�lis sz�m-ot az �sszeg-hez
		dec darab		; cs�kkentj�k a darab sz�ml�l�t

	brne ciklus2		; ha darab=0, akkor k�szen vagyunk

; Az eredm�ny-t be�rjuk a 0x104-es mem�ria c�mre
	ldi XH, 0x01	; 0x104 sz�mot be�rok X fels� c�m�be (BAJLOD�SAN)
	ldi XL, 0x04 	; 0x104 sz�mot be�rok X als�  c�m�be (BAJLOD�SAN)

	st  X+, osszeg	; Az�rt X+, hogy egyb�l tov�bb is ugorjon (BAB!)

stop:
	jmp stop;
